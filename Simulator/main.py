import enum
import random
import sys
import threading


class Messages(enum.Enum):
	"""Just messages that we need in the application."""
	START_MESSAGE = "Welcome to the fishing simulator! You are a real fisherman " \
					"that just arrived at the bank of a glittering river. It is " \
					"a sunny morning and you are ready to begin fishing."
	NO_FISH = "You have no fish!"
	X_FISH = "You have {} fish."
	HINT_FLOAT_IDLE = "The float is still."
	HINT_FLOAT_WOBBLING = "The float is wobbling..."
	HINT_FLOAT_BOUNCING = "The float is bouncing up and down..."
	HINT_FLOAT_SUNKEN = "The float went under!"
	CANT_REEL_NOT_FISHING = "Not fishing, use 'fish' to start fishing."
	ALREADY_FISHING = "Already fishing!"


class WorldState(enum.Enum):
	"""Represents the current world state."""
	IDLE = 0
	FISHING = 1
	ENDED = 10


class FloatState(enum.Enum):
	"""
	Represents the float's (not number but thing that shows whether a fish is
	on the hook) state.
	"""
	IDLE = 0
	WOBBLING = 1
	BOUNCING = 2
	SUNKEN = 3


class Commands(enum.Enum):
	"""Represents commands that the user can enter."""
	FISH = "fish"
	REEL = "reel"
	EXIT = "exit"
	QUIT = "quit"
	COUNT_FISH = "count-fish"

	@staticmethod
	def by_string(string):
		for val in Commands:
			if val.value == string:
				return val


class UserInputThread(threading.Thread):
	"""Separate thread to get user input."""

	def __init__(self):
		super(UserInputThread, self).__init__()
		self.uinput = None

	def run(self):
		global _USER_INPUT_BUFFER, _has_user_prompt
		_has_user_prompt = True
		while True:
			_USER_INPUT_BUFFER.append(input('# '))


# Globals
WORLD_STATE = WorldState.IDLE
FISH_COUNT = 0
FLOAT_STATE = FloatState.IDLE
FLOAT_CHANGE_TIME_LIMITS = (3, 5)  # min_time, max_time
_USER_INPUT_BUFFER = []
_rand_float_timer = None
_has_user_prompt = False


def start(begin_loop=False):
	"""Start the program."""
	global WORLD_STATE, FISH_COUNT
	WORLD_STATE = WorldState.IDLE
	FISH_COUNT = 0

	print(Messages.START_MESSAGE.value)

	if begin_loop:
		input_thread = UserInputThread()
		input_thread.daemon = True
		input_thread.start()

		while True:
			check_input()
			if WORLD_STATE is WorldState.ENDED:
				break


def end():
	"""End the program. Should set the simulator into ENDED state."""
	global WORLD_STATE
	if _rand_float_timer is not None:
		_rand_float_timer.cancel()
	WORLD_STATE = WorldState.ENDED


def check_input():
	"""Check whether we have new input from the user."""
	user_input = get_input()
	if user_input is not None:
		handle_input(user_input)


def get_input():
	"""Get last input by user."""
	if len(_USER_INPUT_BUFFER) > 0:
		return _USER_INPUT_BUFFER.pop(0)


def handle_input(cinput):
	"""Handle user input."""
	global WORLD_STATE, _rand_float_timer, FISH_COUNT
	cmd = Commands.by_string(cinput)

	if cmd is Commands.FISH:
		if WORLD_STATE is WorldState.FISHING:
			_echo_screen(Messages.ALREADY_FISHING.value)
		else:
			WORLD_STATE = WorldState.FISHING
			_start_random_floatstate_generator()
			_echo_screen("You cast your rod into the water")
	elif cmd is Commands.REEL:
		if WORLD_STATE is not WorldState.FISHING:
			_echo_screen(Messages.CANT_REEL_NOT_FISHING.value)
		else:
			WORLD_STATE = WorldState.IDLE
			_rand_float_timer.cancel()

			if generate_fish(FLOAT_STATE):
				FISH_COUNT += 1
				_echo_screen("You caught a fish!")
			else:
				_echo_screen("No fish on hook.")
	elif cmd is Commands.QUIT or cmd is Commands.EXIT:
		_echo_screen("You pack up your stuff and head on home")
		end()
	elif cmd is Commands.COUNT_FISH:
		if FISH_COUNT > 0:
			_echo_screen(Messages.X_FISH.value.format(FISH_COUNT))
		else:
			_echo_screen(Messages.NO_FISH.value)
	else:
		_echo_screen("Unknown action")


def generate_float_state():
	"""Generates a random new state for the fishing float."""
	result = random.uniform(0.0, 1.0)
	if result < 0.25:
		return FloatState.IDLE
	elif result < 0.5:
		return FloatState.WOBBLING
	elif result < 0.75:
		return FloatState.BOUNCING
	else:
		return FloatState.SUNKEN


def trigger_float_change():
	"""Change the fishing float to a random new state."""
	handle_new_float_state(generate_float_state())


def handle_new_float_state(new_state):
	"""Handle new float state."""
	global FLOAT_STATE

	if new_state is FloatState.IDLE:
		_echo_screen(Messages.HINT_FLOAT_IDLE.value)
	elif new_state is FloatState.WOBBLING:
		_echo_screen(Messages.HINT_FLOAT_WOBBLING.value)
	elif new_state is FloatState.BOUNCING:
		_echo_screen(Messages.HINT_FLOAT_BOUNCING.value)
	elif new_state is FloatState.SUNKEN:
		_echo_screen(Messages.HINT_FLOAT_SUNKEN.value)
	FLOAT_STATE = new_state


def generate_fish(float_state):
	"""Generate a fish depending on the float state provided."""
	chance = random.uniform(0, 1.0)
	if float_state is FloatState.IDLE and chance < random.uniform(0, 0.1):
		return True
	elif float_state is FloatState.WOBBLING \
			and chance < random.uniform(0.1, 0.3):
		return True
	elif float_state is FloatState.BOUNCING \
			and chance < random.uniform(0.4, 0.7):
		return True
	elif float_state is FloatState.SUNKEN and chance < random.uniform(0.85, 1):
		return True


def _rand_floatstate_gen_interrupt():
	"""Interrupt for FloatState change event timer."""
	trigger_float_change()
	_start_random_floatstate_generator()


def _start_random_floatstate_generator():
	global _rand_float_timer

	time_ms = random.uniform(*FLOAT_CHANGE_TIME_LIMITS)
	_rand_float_timer = threading.Timer(
		time_ms, _rand_floatstate_gen_interrupt, [])
	_rand_float_timer.start()


def _echo_screen(txt):
	"""Special function to allow printing to the screen."""
	if _has_user_prompt:
		print('\r' + txt)
		print('# ', end='')
		sys.stdout.flush()
	else:
		print(txt)


if __name__ == '__main__':
	start(begin_loop=True)

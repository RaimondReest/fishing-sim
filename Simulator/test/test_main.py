import io
import time
import unittest.mock

import main


class TestSimulation(unittest.TestCase):
	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_start_msg_printed(self, mock_stdout):
		"""Test that the simulator prints the start message on start"""
		main.start()
		self.assertEqual(
			main.Messages.START_MESSAGE.value + "\n",
			mock_stdout.getvalue())

	def test_initial_state(self):
		"""Test the initial state of the simulation."""
		main.start()
		self.assertEqual(main.WorldState.IDLE, main.WORLD_STATE)
		self.assertEqual(main.FISH_COUNT, 0)

	def test_end(self):
		"""Test that the simulation can end."""
		main.end()
		self.assertEqual(main.WorldState.ENDED, main.WORLD_STATE)

	@unittest.mock.patch('main.get_input', return_value='command')
	@unittest.mock.patch('main.handle_input')
	def test_insert_input(self, mock_handle_input, _):
		"""Test that the program theoretically knows how to pipe user input."""
		main.check_input()
		mock_handle_input.assert_called_with("command")

	def test_fish_command(self):
		"""Test the fish command."""
		main.handle_input(main.Commands.FISH.value)
		self.assertEqual(main.WorldState.FISHING, main.WORLD_STATE)

		with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as si:
			main.handle_input(main.Commands.FISH.value)
			self.assertEqual(
				main.Messages.ALREADY_FISHING.value + '\n',
				si.getvalue())

	def test_reel_command(self):
		"""Test the reel command."""
		main.handle_input(main.Commands.FISH.value)
		main.handle_input(main.Commands.REEL.value)
		self.assertEqual(main.WorldState.IDLE, main.WORLD_STATE)

		with unittest.mock.patch('sys.stdout', new_callable=io.StringIO) as si:
			main.handle_input(main.Commands.REEL.value)
			self.assertEqual(
				main.Messages.CANT_REEL_NOT_FISHING.value + '\n',
				si.getvalue())

	def test_exit_command(self):
		"""Test the exit command."""
		main.start()
		main.handle_input(main.Commands.EXIT.value)
		self.assertEqual(main.WorldState.ENDED, main.WORLD_STATE)

	def test_quit_command(self):
		"""Test the quit command."""
		main.start()
		main.handle_input(main.Commands.QUIT.value)
		self.assertEqual(main.WorldState.ENDED, main.WORLD_STATE)

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_count_fish_no_fish(self, mock_stdout):
		"""Test the count fish command when we have no fish."""
		main.FISH_COUNT = 0
		main.handle_input(main.Commands.COUNT_FISH.value)
		self.assertEqual(
			main.Messages.NO_FISH.value + '\n',
			mock_stdout.getvalue())

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_count_fish(self, mock_stdout):
		"""Test the count fish command given we have 5 fish in the bucket."""
		main.FISH_COUNT = 5
		main.handle_input(main.Commands.COUNT_FISH.value)
		self.assertEqual(
			main.Messages.X_FISH.value.format(main.FISH_COUNT) + '\n',
			mock_stdout.getvalue())

	@unittest.mock.patch(
		'main.generate_float_state',
		return_value=main.FloatState.WOBBLING)
	def test_float_change_event(self, mock_generator):
		"""
		Tests that the simulator is capable of generating a new float state
		at pre-set times.
		"""
		main.FLOAT_STATE = main.FloatState.IDLE
		main.FLOAT_CHANGE_TIME_LIMITS = (0.2, 0.2)
		main.handle_input(main.Commands.FISH.value)  # Start fishing.
		time.sleep(0.24)
		self.assertIs(main.FloatState.WOBBLING, main.FLOAT_STATE)

		# See that it generates the event again
		mock_generator.return_value = main.FloatState.SUNKEN
		time.sleep(0.24)
		self.assertIs(main.FloatState.SUNKEN, main.FLOAT_STATE)

		# See that it'll stop once the hook is reeled in
		main.handle_input(main.Commands.REEL.value)
		mock_generator.return_value = main.FloatState.BOUNCING
		time.sleep(0.24)
		self.assertIsNot(main.FloatState.BOUNCING, main.FLOAT_STATE)

		main.FLOAT_CHANGE_TIME_LIMITS = (0.1, 0.1)
		mock_generator.return_value = main.FloatState.BOUNCING
		main.handle_input(main.Commands.FISH.value)
		time.sleep(0.15)
		try:
			self.assertIs(main.FloatState.BOUNCING, main.FLOAT_STATE)
		except AssertionError as ex:
			main.handle_input(main.Commands.REEL.value)
			raise ex
		main.handle_input(main.Commands.REEL.value)

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_float_hint_idle(self, mock_stdout):
		main.handle_new_float_state(main.FLOAT_STATE.IDLE)
		self.assertEqual(
			main.Messages.HINT_FLOAT_IDLE.value + '\n',
			mock_stdout.getvalue())

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_float_hint_wobbling(self, mock_stdout):
		main.handle_new_float_state(main.FLOAT_STATE.WOBBLING)
		self.assertEqual(
			main.Messages.HINT_FLOAT_WOBBLING.value + '\n',
			mock_stdout.getvalue())

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_float_hint_bouncing(self, mock_stdout):
		main.handle_new_float_state(main.FLOAT_STATE.BOUNCING)
		self.assertEqual(
			main.Messages.HINT_FLOAT_BOUNCING.value + '\n',
			mock_stdout.getvalue())

	@unittest.mock.patch('sys.stdout', new_callable=io.StringIO)
	def test_float_hint_sunken(self, mock_stdout):
		main.handle_new_float_state(main.FLOAT_STATE.SUNKEN)
		self.assertEqual(
			main.Messages.HINT_FLOAT_SUNKEN.value + '\n',
			mock_stdout.getvalue())

	def test_statistical_floatstate_generator(self):
		"""
		Test the statistical output of float state generation. Doesn't actually
		test anything, just statistics for reference.
		"""
		results = [main.generate_float_state() for _ in range(100000)]
		sunken = list(filter(lambda l: l is main.FloatState.SUNKEN, results))
		idle = list(filter(lambda l: l is main.FloatState.IDLE, results))
		wobbling = list(
			filter(lambda l: l is main.FloatState.WOBBLING, results))
		bouncing = list(
			filter(lambda l: l is main.FloatState.BOUNCING, results))

		print(
			'Float gen statistics: idle {}%, wobbling {}%, bouncing {}%, sunken {}%'
				.format(len(idle) / len(results) * 100,
						len(wobbling) / len(results) * 100,
						len(bouncing) / len(results) * 100,
						len(sunken) / len(results) * 100)
		)

	@unittest.mock.patch('main.generate_fish', return_value=True)
	def test_fish_reeling(self, fish_generator):
		main.handle_input(main.Commands.FISH.value)
		main.handle_input(main.Commands.REEL.value)
		self.assertEqual(1, main.FISH_COUNT)

		main.FISH_COUNT = 0
		fish_generator.return_value = False
		main.handle_input(main.Commands.FISH.value)
		main.handle_input(main.Commands.REEL.value)
		self.assertEqual(0, main.FISH_COUNT)

	@unittest.mock.patch(
		'main.generate_float_state',
		return_value=main.FloatState.WOBBLING)
	def test_fish_and_exit(self, _):
		"""Test that the simulator wont keep fishing when exiting mid-fish."""
		main.FLOAT_CHANGE_TIME_LIMITS = (0.1, 0.1)
		main.handle_input(main.Commands.FISH.value)
		main.handle_input(main.Commands.EXIT.value)
		time.sleep(0.15)
		self.assertIsNot(main.FloatState.WOBBLING, main.FLOAT_STATE)

	def tearDown(self):
		"""Clean up the tests."""
		super().tearDown()
		# Make sure we don't get stuck in "fishing" state.
		main.handle_input(main.Commands.REEL.value)
		main.FISH_COUNT = 0

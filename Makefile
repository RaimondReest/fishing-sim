all:
	pyinstaller Simulator/main.py --onefile --name fishing-sim

clean:
	rm -rf build dist main.spec __pycache__
